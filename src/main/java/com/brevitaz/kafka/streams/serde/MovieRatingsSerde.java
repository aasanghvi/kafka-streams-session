package com.brevitaz.kafka.streams.serde;

import com.brevitaz.kafka.streams.RatedMovie;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MovieRatingsSerde
{
	public static SpecificAvroSerde<RatedMovie> ratedMovieAvroSerde(final Map<String, String> serdeConfig)
	{
		final SpecificAvroSerde<RatedMovie> movieAvroSerde = new SpecificAvroSerde<>();
		movieAvroSerde.configure(serdeConfig, false);
		return movieAvroSerde;
	}
}
