package com.brevitaz.kafka.streams.mapper;

import com.brevitaz.kafka.streams.Movie;
import com.brevitaz.kafka.streams.RatedMovie;
import com.brevitaz.kafka.streams.Rating;
import org.apache.kafka.streams.kstream.ValueJoiner;

public class MovieRatingJoiner implements ValueJoiner<Rating, Movie, RatedMovie>
{
	public RatedMovie apply(Rating rating, Movie movie)
	{
		return RatedMovie.newBuilder()
				.setId(movie.getId())
				.setTitle(movie.getTitle())
				.setReleaseYear(movie.getReleaseYear())
				.setRating(rating.getRating())
				.build();
	}
}
