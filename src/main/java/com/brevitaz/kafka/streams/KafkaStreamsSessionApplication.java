package com.brevitaz.kafka.streams;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class KafkaStreamsSessionApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaStreamsSessionApplication.class, args);
	}

}
