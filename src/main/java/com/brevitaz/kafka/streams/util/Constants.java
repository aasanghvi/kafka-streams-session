package com.brevitaz.kafka.streams.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants
{

	public static final String COMMA = ",";
	public static final String SCHEMA_REGISTRY_URL_PROPERTY = "schema.registry.url";
	public static final String BOOTSTRAP_SERVERS = "bootstrap.servers";

	// FAV COLOUR APP
	public static final String FAV_COLOUR_INPUT_TOPIC = "favourite-colour-input";
	public static final String FAV_COLOUR_USR_KEYS_AND_COLOUR_INTERIM_TOPIC = "user-keys-and-colours";
	public static final String FAV_COLOUR_COUNTS_BY_COLOURS_STORE = "CountsByColours";
	public static final String FAV_COLOUR_OUTPUT_TOPIC = "favourite-colour-output";
	public static final String FAV_COLOUR_APP_ID = "Fav-Color-App";

	// MOVIE RATINGS APP
	public static final String MOVIE_RATINGS_APP_ID = "Movie-Ratings-App";
	public static final String MOVIE_TOPIC_NAME = "movies";
	public static final String REKEYED_MOVIE_TOPIC_NAME = "rekeyed-movies";
	public static final String RATING_TOPIC_NAME = "ratings";
	public static final String RATED_MOVIES_TOPIC_NAME = "rated-movies";

}
