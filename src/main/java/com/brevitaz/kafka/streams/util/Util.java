package com.brevitaz.kafka.streams.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.ForeachAction;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Util
{

	public static <T, U> ForeachAction<T, U> logMessageKeyAndValue(String event)
	{
		return (key, value) -> log.debug(event + " with key={} and value={}", key, value);
	}

}
