package com.brevitaz.kafka.streams.starter;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.Topology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;


public interface IStreamStarter
{
	Logger log = LoggerFactory.getLogger(IStreamStarter.class);

	KafkaStreams startStream(Topology topology, Properties streamsConfig);

	default void printThreadMetadata(@NonNull KafkaStreams streams)
	{
		streams.localThreadsMetadata().forEach(threadMetaData -> log.debug("ThreadMetadata={}", threadMetaData));
	}

	default void shutDown(@NonNull KafkaStreams streams, CountDownLatch latch, String id)
	{
		Runtime.getRuntime()
				.addShutdownHook(new Thread(() -> {
					log.info("Gracefully closing the {} stream", id);
					streams.close();
					latch.countDown();
				}));
	}

	Topology getTopology();

	Properties getStreamsConfig();
}
