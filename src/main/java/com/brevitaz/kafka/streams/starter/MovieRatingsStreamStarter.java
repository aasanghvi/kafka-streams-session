package com.brevitaz.kafka.streams.starter;

//@Component
//@Slf4j
public class MovieRatingsStreamStarter //implements IStreamStarter
{
//	private static final MovieRatingJoiner joiner = new MovieRatingJoiner();
//	private final MovieRatingStreamConfig streamConfig;
//
//	@Value("${spring.kafka.schema-registry}")
//	private String schemaRegistry;
//
//	public MovieRatingsStreamStarter(final MovieRatingStreamConfig streamConfig)
//	{
//		this.streamConfig = streamConfig;
//	}
//
//	@Override
//	@Bean("startMovieRatingsStream")
//	public KafkaStreams startStream(@Qualifier("movieRatingsTopology") final Topology topology,
//	                                @Qualifier("movieRatingsStreamConfig") final Properties streamsConfig)
//	{
//
//		final KafkaStreams streams = new KafkaStreams(topology, streamsConfig);
//
//		final CountDownLatch latch = new CountDownLatch(1);
//
//		log.debug("starting movieRatings KafkaStreams");
//		try
//		{
//			streams.start();
//			latch.await();
//		} catch (InterruptedException e)
//		{
//			log.error("Exception encountered when CountdownLatch={} interrupted", latch, e);
//			System.exit(1);
//		}
//		catch (Exception e)
//		{
//			log.error("Exception encountered at streams with errorMessage={}", e.getMessage(), e);
//			System.exit(1);
//		}
//
//		printThreadMetadata(streams);
//
//		shutDown(streams, latch, Constants.MOVIE_RATINGS_APP_ID);
//
//		return streams;
//
//	}
//
//	@Override
//	@Bean("movieRatingsTopology")
//	public Topology getTopology()
//	{
//		final Map<String, String> serdeConfig = Collections.singletonMap(SCHEMA_REGISTRY_URL_PROPERTY, schemaRegistry);
//		SpecificAvroSerde<RatedMovie> ratedMoveSerde = MovieRatingsSerde.ratedMovieAvroSerde(serdeConfig);
//
//		log.debug("loading movie rating topology");
//		return buildTopology(ratedMoveSerde);
//	}
//
//	@Bean("movieRatingsStreamConfig")
//	@Override
//	public Properties getStreamsConfig()
//	{
//		log.debug("creating movie rating topics");
//		streamConfig.createTopics();
//
//		log.debug("loading movie rating streams config");
//		return streamConfig.getStreamConfig();
//	}
//
//	public Topology buildTopology(SpecificAvroSerde<RatedMovie> ratedMoveSerde)
//	{
//		final StreamsBuilder builder = new StreamsBuilder();
//
//		KStream<String, Movie> movieStream = builder
//				.<String, Movie>stream(MOVIE_TOPIC_NAME)
//				.map((key, movie) -> new KeyValue<>(movie.getId().toString(), movie))
//				.peek(Util.logMessageKeyAndValue("read message from movie topic"));
//
//		movieStream.to(REKEYED_MOVIE_TOPIC_NAME);
//
//		KTable<String, Movie> movies = builder.table(REKEYED_MOVIE_TOPIC_NAME);
//
//		KStream<String, Rating> ratings = builder.<String, Rating>stream(RATING_TOPIC_NAME)
//				.map((key, rating) -> new KeyValue<>(rating.getId().toString(), rating))
//				.peek(Util.logMessageKeyAndValue("read message from ratings topic"));
//
//		KStream<String, RatedMovie> ratedMovie = ratings.join(movies, joiner)
//				.peek(Util.logMessageKeyAndValue("joined message from ratedMovie topic"));
//
//		ratedMovie.to(RATED_MOVIES_TOPIC_NAME, Produced.with(Serdes.String(), ratedMoveSerde));
//
//		return builder.build();
//	}


}
