package com.brevitaz.kafka.streams.starter;

import com.brevitaz.kafka.streams.config.streamconfig.FavColourStreamConfig;
import com.brevitaz.kafka.streams.util.Constants;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import static com.brevitaz.kafka.streams.util.Constants.*;
import static com.brevitaz.kafka.streams.util.Util.logMessageKeyAndValue;


@Component
@Slf4j
@AllArgsConstructor
public class FavColorStreamStarter implements IStreamStarter
{
	private final FavColourStreamConfig streamConfig;

	@Override
	@Bean("startFavColorStream")
	public KafkaStreams startStream(@Qualifier("favColorsTopology") final Topology topology,
	                                @Qualifier("favColorsStreamConfig") final Properties streamsConfig)
	{

		final KafkaStreams streams = new KafkaStreams(topology, streamsConfig);

		final CountDownLatch latch = new CountDownLatch(1);

		log.debug("starting Fav Color KafkaStreams");

		try
		{
			streams.start();
			latch.await();
		} catch (InterruptedException e)
		{
			log.error("Exception encountered when CountdownLatch={} interrupted", latch, e);
			System.exit(1);
		}

		printThreadMetadata(streams);

		shutDown(streams, latch, Constants.FAV_COLOUR_APP_ID);

		return streams;

	}

	@Override
	@Bean("favColorsTopology")
	public Topology getTopology()
	{
		log.debug("loading Fav Colour topology");
		return buildTopology();
	}

	@Override
	@Bean("favColorsStreamConfig")
//	@PostConstruct
	public Properties getStreamsConfig()
	{
		log.debug("loading Fav colour streams config");
		return streamConfig.getStreamConfig();
	}

	public Topology buildTopology()
	{
		StreamsBuilder builder = new StreamsBuilder();

		// Step 1: We create the topic of users keys to colours
		KStream<String, String> textLines = builder.stream(FAV_COLOUR_INPUT_TOPIC);

		KStream<String, String> usersAndColours = textLines
				.filter((key, value) -> value.contains(COMMA)) // 1 - we ensure that a comma is here as we will split on it
				.selectKey((key, value) -> value.split(COMMA)[0].toLowerCase()) // 2 - we select a key that will be the user id (lowercase for safety)
				.mapValues(value -> value.split(COMMA)[1].toLowerCase()) // 3 - we get the colour from the value (lowercase for safety)
				.filter((user, colour) -> Arrays.asList("green", "blue", "red").contains(colour)) // 4 - we filter undesired colours (could be a data sanitization step)
				.peek(logMessageKeyAndValue("final message"));

		usersAndColours.to(FAV_COLOUR_USR_KEYS_AND_COLOUR_INTERIM_TOPIC);

		Serde<String> stringSerde = Serdes.String();
		Serde<Long> longSerde = Serdes.Long();

		// step 2 - we read that topic as a KTable so that updates are read correctly
		KTable<String, String> usersAndColoursTable = builder.table(FAV_COLOUR_USR_KEYS_AND_COLOUR_INTERIM_TOPIC);

		// step 3 - we count the occurrences of colours
		KTable<String, Long> favouriteColours = usersAndColoursTable
				// 5 - we group by colour within the KTable
				.groupBy((user, colour) -> new KeyValue<>(colour, colour))
				.count(Materialized.<String, Long, KeyValueStore<Bytes, byte[]>>as(FAV_COLOUR_COUNTS_BY_COLOURS_STORE)
						.withKeySerde(stringSerde)
						.withValueSerde(longSerde));

		// 6 - we output the results to a Kafka Topic - don't forget the serializers
		favouriteColours.toStream().to(FAV_COLOUR_OUTPUT_TOPIC, Produced.with(Serdes.String(), Serdes.Long()));

		return builder.build();
	}

}
