
package com.brevitaz.kafka.streams.config.streamconfig;


import com.brevitaz.kafka.streams.util.Constants;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;


@Configuration
public class FavColourStreamConfig
{
	@Value("${spring.kafka.bootstrap-servers}")
	private String bootStrapServers;

	public Properties getStreamConfig()
	{
		Properties config = new Properties();

		config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
		config.put(StreamsConfig.APPLICATION_ID_CONFIG, Constants.FAV_COLOUR_APP_ID);
		config.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG, LogAndContinueExceptionHandler.class);
		config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		config.put(StreamsConfig.RETRIES_CONFIG, 5);
		config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		config.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");
		return config;
	}
}
